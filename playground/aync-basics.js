console.log("Starting app");
setTimeout(() => {
  console.log("First Callback, 2000ms");
}, 2000)

setTimeout(() => {
  console.log("Second Callback, 0ms");
}, 0)

console.log("Ending app");
