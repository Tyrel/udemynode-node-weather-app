var asyncAdd = (a, b) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (typeof a === 'number' && typeof b === 'number'){
        resolve(a + b);
      } else {
        reject("Arguments must be numbers");
      }
    }, 500);

  })
}
//
//
// var somePromise = new Promise((resolve, reject) => {
//   setTimeout(() => {
//     // resolve("Hey, It worked!");
//     reject("FAILED BRUH");
//   }, 500);
// });
//
// somePromise.then((message) => {
//   console.log("Success:", message);
// },
// (errorMessage) => {
//   console.log("Error:", errorMessage);
// })


asyncAdd(1, 2).then((res) => {
  return asyncAdd(res, 33);
}).then((res) => {
  console.log("Should be 36", res);
}).catch((errorMessage) => {
  console.log(errorMessage);
})
